Filtonary decomposition
=======================

This class implements the sparse decomposition of an image by a set of learned filters.
It also provides the filter learning scheme.

See the LaunchMe file and the help of each methods.

Author: Guillaume Tartavel
See also: my thesis, Chap. III


# Content

    -  readme.md                -- you are reading it
    -  Filtonary.m              -- main class
    -  LaunchMe.m               -- an example of learning and decomposition
    -  projL2w.m                -- auxiliary function: projection on an ellipsis
    -  Fabric.0002.png          -- a sample image
    +- improc_toolbox/          -- replacement functions
    |   +  <files>.m            --     for the image processing toolbox
