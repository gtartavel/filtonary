classdef Filtonary
% FILTONARY     - filtonary learning class
%
%   (c) Guillaume Tartavel, 2012--2014
   
    % Properties
    properties (Access=public)
        ker;
    end;
    
    properties (Constant)
        inside = false;
        display = false;
    end;
    
    properties (Dependent)
        Nker;
        Nc;
    end;
    
    % Methods: learning & decomposition
    methods (Access=public)
        
        function this = Filtonary(ker)
        % FILTONARY     - constructor
        %
        %   Usage:
        %       F = Filtonary()
        %       F = Filtonary(kernel)
        %       F = Filtonary(kernels)
        
            if(~exist('ker', 'var'))
                this.ker = [];
            else
                this.ker = ker;
            end;
        end;
        
        function [this, W, imRec] = learn(this, im, lambda, nIter, W)
        % LEARN     - learn a filtonary for an image
        %
        %   F = F.learn(im, lambda, nIter);
        %   F = F.learn(im, lambda, nIter, W);
        %   [F, W, rec] = F.learn(...);
        
            % Initialize
            [Nx,Ny,Nc] = size(im);
            [Mx,My,Mc,K] = size(this.ker);
            if (Nc ~= Mc)
                error('Cannot mix color and gray-level.');
            end;
            if (exist('W', 'var'))
                R = im - this.reconstruct(W);
            else
                B = ([Mx My] - 1) * (2 * Filtonary.inside - 1);
                W = zeros(Nx-B(1), Ny-B(2), 1, K);
                R = im;
            end;
            scores = zeros(nIter, 2);
            
            % Functions
            gauss = @(N) exp(-2 * ( (0:N-1)/(N-1) - 1/2 ).^2);  % prev: 9
            normL1 = @(w) sum(abs(w(:)));
            normL2_2 = @(w) sum(w(:) .^ 2);
            L2w = 1 ./ sqrt(bsxfun(@times, gauss(Mx)', gauss(My)));
            L2w = repmat(L2w / sqrt(normL2_2(L2w)), [1 1 Nc]);
            proxL1 = @(w, t) Filtonary.Threshold(w, t * lambda);
            %proxL2 = @(w, t) w / (sqrt(normL2_2(w)) + eps); disp('Non-weighted');
            proxL2 = @(w, t) projL2w(w, L2w);
            regularity = @(w) lambda * normL1(w);
            psnr = @(R) -10 * log10(mean(R(:) .^ 2));
            score = @(R, W) 1/2 * normL2_2(R) + regularity(W);
            
            % Display
            if (Filtonary.display)
                figure;
                if (Nc < 3)
                    colormap(gray);
                end;
                subplot 231; imshow(im); title('Image'); drawnow;
            end;
            
            % Solve
            for n = 1:nIter
              for k = randperm(K)
                Wk = W(:,:,:,k);
                hk = this.ker(:,:,:,k);
                
                % Optimize W
                [Wk, R] = Filtonary.proxGradStep(Wk, hk, R, proxL1, regularity);      

                % Update
                W(:,:,:,k) = Wk;
                scores(n, 1) = score(R, W);
                
                % Optimize D
                [hk, R] = Filtonary.proxGradStep(hk, Wk, R, proxL2);

                % Update
                this.ker(:,:,:,k) = hk;
                scores(n, 2) = score(R, W);
                
                % Display
                if (Filtonary.display)
                    subplot 232; imshow(im - R);
                    title(sprintf('Reconstruction, %.2f dB', psnr(R)));
                    %subplot 233;
                    subplot 234; imagesc(Wk);
                        title(sprintf('Decomposition #%d', n));
                    subplot 235; imagesc(abs(hk) / max(hk(:)));
                        title(sprintf('Filtom #%d', k));
                    subplot 236; cla; title('Score'); hold on;
                        plot((1:n)-1/2, scores(1:n, 1), '.'); plot(1:n, scores(1:n, 2), 'o');
                        axis tight; xlim([1, nIter]);
                    drawnow;
                end;
              end;
            end;
            imRec = im - R;
            
        end;
        
        function [W, imRec] = decompose(this, im, lambda, nIter)
        % DECOMPOSE     - decompose an image in a Filtonary.
        %
        %   Cost function:
        %       J(W) = 1/2 ||im - rec(W)||_2^2 + lambda * ||W||_1
        %   
        %   Usage:
        %       W = F.decompose(im, lambda, nIter);
        %       [W, imRec, DW] = F.decompose(...);
        %   
        %   Note:
        %       DWs is the adjoint of W derivative function in W.
        %       It is called by: DWs(dW) for any weights dW.
        
            % Functions
            normL1 = @(w) sum(abs(w(:)));
            normL2_2 = @(w) sum(w(:) .^ 2);
            proxL1 = @(w, t) Filtonary.Threshold(w, t * lambda);
            regularity = @(w) lambda * normL1(w);
            psnr = @(R) -10 * log10(mean(R(:) .^ 2));
            score = @(R, W) 1/2 * normL2_2(R) + regularity(W);
            
            % Initialize
            [Nx,Ny,Nc] = size(im);
            [Mx,My,Mc,K] = size(this.ker);
            if (Nc ~= Mc)
                error('Cannot mix color and gray-level.');
            end;
            B = ([Mx My] - 1) * (2 * Filtonary.inside - 1);
            W = zeros(Nx-B(1), Ny-B(2), 1, K);
            R = im;
            scores = zeros(nIter, 1);
                   
            % Display
            if (Filtonary.display)
                figure;
                if (Nc < 3)
                    colormap(gray);
                end;
                subplot 231; imshow(im); title('Input'); drawnow;
            end;
            
            % Solve
            for n = 1:nIter
              for k = randperm(K)
                Wk = W(:,:,:,k);
                hk = this.ker(:,:,:,k);

                % Optimize
                [Wk, R] = Filtonary.proxGradStep(Wk, hk, R, proxL1, regularity);

                % Update
                W(:,:,:,k) = Wk;
                scores(n) = score(R, W);

                % Display
                if (Filtonary.display)
                    subplot 232; imshow(im - R);
                        title(sprintf('Reconstruction, %.1f dB', psnr(R)));
                    subplot 234; imagesc(Wk); axis tight;
                        title(sprintf('Decomposition #%d', n));
                    subplot 235; imagesc(abs(hk) / max(hk(:))); axis tight;
                        title(sprintf('Filtom #%d', k));
                    subplot 236; cla; plot(scores(1:n), '.-'); title('Score');
                        axis tight; xlim([1, nIter]);
                    drawnow;
                end;
              end;
            end;
            imRec = im - R;
        end;
        
        function [W, imRec] = optimize(this, im, W, nIter)
        % OPTIMIZE  - optimize the weights on their support.
        %
        %   Cost function:
        %       J(W) = 1/2 ||im - rec(W)||_2^2 -- on the support of W
        %
        %   Usage:
        %       [W, imRec] = F.optimize(im, W, nIter);
        
            % Initialize
            K = this.Nker;
            psnr = @(R) -10 * log10(mean(R(:) .^ 2));
            score = @(R) 1/2 * sum(R(:) .^ 2);
            imRec = this.reconstruct(W);
            Wbool = (W ~= 0);
            R = im - imRec;
            scores = zeros(nIter, 1);
            
            % Display
            if (Filtonary.display)
                figure;
                if (this.Nc < 3)
                    colormap(gray);
                end;
                subplot 231; imshow(im); title('Input'); drawnow;
            end;
            
            % Solve
            for n = 1:nIter
              for k = randperm(K)
                Wk = W(:,:,:,k);
                hk = this.ker(:,:,:,k);

                % Optimize
                proj = @(Xk, t) Xk .* Wbool(:,:,:,k);
                [Wk, R] = Filtonary.proxGradStep(Wk, hk, R, proj);

                % Update
                W(:,:,:,k) = Wk;
                scores(n) = score(R);
                
                % Display
                if (Filtonary.display)
                    subplot 232; imshow(im - R);
                        title(sprintf('Reconstruction, %.1f dB', psnr(R)));
                    subplot 234; imagesc(Wk); axis tight;
                        title(sprintf('Decomposition #%d', n));
                    subplot 235; imagesc(abs(hk) / max(hk(:))); axis tight;
                        title(sprintf('Filtom #%d', k));
                    subplot 236; cla; plot(scores(1:n), '.-'); title('Score');
                        axis tight; xlim([1, nIter]);
                    drawnow;
                end;
              end;
            end;
            imRec = im - R;
        end;
        
        function im = reconstruct(this, W)
        % RECONSTRUCT   - reconstruct the image
        %
        %   Usage:
        %       im = F.reconstruct(W);
            im = Filtonary.conv(W, this.ker, ~Filtonary.inside);
        end;
        
    end;
    
    % Tools
    methods (Static)
        
        function y = Threshold(x, t)
        % THRESHOLD   - thesholding function
        %
        %   Usage:
        %       y = Threshold(x, t);
        
            %y = max(x - t, 0);                      % Soft, non-neg
            y = x .* max(1 - t ./ abs(x), 0);     	% Soft
            %y = x .* max(1 - t^2 ./ x.^2, 0);       % Stein, bad converg.
        end;
        
        function y = conv(x, h, crop)
        % CONV      - fast 2D convolution using Fourier domain
        %
        %   Usage:
        %       y = Filtonary.conv(x, h);
        %       y = Filtonary.conv(x, h, crop);
        %
        %   Output size:
        %       Dimension 1 and 2:
        %           Default: size(y) = size(x) + size(h) - 1.
        %           If crop: size(y) = size(x) - size(h) + 1.
        %       Dimension 3 and 4:
        %           
        
            % Size
            [Mx,Nx,Px,Kx] = size(x);
            [Mh,Nh,Ph,Kh] = size(h);
            M = Mx + Mh - 1;
            N = Nx + Nh - 1;
            
            % Check 3rd and 4th dimension
            if (Px ~= Ph && min(Px,Ph) ~= 1)
                error('Inconsistent 3rd dimension.');
            end;
            if (Kx ~= Kh && min(Kx,Kh) ~= 1)
                error('Inconsistent 4th dimension.');
            end;
            
            % Convolve
            X = fft2(x, M, N);
            H = fft2(h, M, N);
            Y = bsxfun(@times, X, H);
            y = ifft2(Y, 'symmetric');
            
            % Reduce 3rd and 4th dimension
            if (min(Px,Ph) > 1)
                y = sum(y, 3);
            end;
            if (min(Kx,Kh) > 1)
                y = sum(y, 4);
            end;
            
            % Crop
            if (exist('crop', 'var') && crop)
                y = y(Mh:end-Mh+1, Nh:end-Nh+1, :, :);
            end;
        end;
        
        function [Xf, Rf] = proxGradStep(X, A, R, prox, regularity, extrapolation)
        % PROX GRAD STEP    - a step of Forward-Backward algorithm
        %
        %   Usage:
        %       [X', R'] = proxGradStep(X, A, R, prox);
        %       [X', R'] = proxGradStep(X, A, R, prox, regularity);
        %
        %   Functional to be minimized:
        %       J(X) = 1/2 ||Y - A*X||^2 + regularity(X)
        %
        %   Arguments
        %       X       - variable
        %       A       - convolutive constant
        %       R       - residual Y - A*X
        %       P       - proximal operator for regularity, prox(X, t)
        %       reg     - regularity function, if any
        %       extrapo - extrapolation factor, between 0 and 1
        %                 convergence is not ensured, but algo. is faster
        
            % Computation
            if (size(R, 1) >= size(A, 1))
                dir_conv = @(u, v) Filtonary.conv(u, v, Filtonary.inside);
                adj_conv = @(u, v) Filtonary.conv(u, v, ~Filtonary.inside);
            else
                dir_conv = @(u, v) Filtonary.conv(v, u, true);
                adj_conv = @(u, v) Filtonary.conv(v, u, true);
            end;
            As = flipdim(flipdim(A, 1), 2);     % A*, i.e. reversed
            d = dir_conv(R, As);                % Descent direction
            t = 1 / (sum(abs(A(:)))^2 + eps);   % First descent step
            
            % Functions
            if (~exist('regularity', 'var')), regularity = @(X) 0; end;
            newR = @(newX) R - adj_conv(newX - X, A);
            score = @(X, R) sum(R(:).^2)/2 + regularity(X);
            
            % First step
            Xf = prox(X + t*d, t);
            Rf = newR(Xf);
            tf = t;
            
            % Find best step
            found = false;
            while (~found)
                
                % Compute next candidate
                t = 2 * tf;
                Xt = prox(X + t*d, t);
                Rt = newR(Xt);
                
                % Validate it or stop
                if (score(Xt, Rt) < score(Xf, Rf))
                    Xf = Xt;
                    Rf = Rt;
                    tf = t;
                else
                    found = true;
                end;
            end;
       
            % Extrapolation
            if (exist('extrapolation', 'var'))
                mu = 1 + extrapolation;
                Xf = mu * Xf + (1 - mu) * X;
                Rf = mu * Rf + (1 - mu) * R;
            end;
        end;
   
        function [Xf, Yf, Rf] = FistaStep(X, Y, n, A, R, prox, regularity)
        % FISTA STEP    - a step of the FISTA algorithm - L1 regularization
        %
        %   See:
        %       proxGradStep
        
            % Functions
            if (~exist('regularity', 'var')), regularity = @(X) 0; end;
            newR = @(newX) R - Filtonary.conv(newX - X, A, ~Filtonary.inside);
            score = @(X, R) sum(R(:).^2)/2 + regularity(X);
            
            % Computation
            As = flipdim(flipdim(A, 1), 2);             % A*, i.e. reversed
            d = Filtonary.conv(newR(Y), As, Filtonary.inside);      % Descent direction
            t = 1 / (sum(abs(A(:)))^2 + eps);           % First descent step
            
            % Step
            Xf = prox(Y + t*d, t);
            Rf = newR(Xf);
            
            % Find best step
            found = false;
            while (~found)
                
                % Compute next candidate
                t = 2 * t;
                Xt = prox(Y + t*d, t);
                Rt = newR(Xt);
                
                % Validate it or stop
                if (score(Xt, Rt) < score(Xf, Rf))
                    Xf = Xt;
                    Rf = Rt;
                else
                    found = true;
                end;
            end;
            
            % Update auxiliary variable
            Yf = Xf + n / (n+3) * (Xf - X);
        end;
        
    % Others
        function [imVar, imMean] = removeMean(im, patchSize)
        % REMOVE MEAN       - decompose the image as mean + variations
        %
        %   Usage:
        %       [imVar, imMean] = Filtonary.removeMean(im, patchSize);
        
            % Filter
            h = ones(patchSize, patchSize, size(im, 3));
            imh = Filtonary.conv(im, h, Filtonary.inside);
            onesh = Filtonary.conv(ones(size(im)), h, Filtonary.inside);
            
            % Inverse filter
            imrec = Filtonary.conv(imh, h, ~Filtonary.inside);
            weights = Filtonary.conv(onesh, h, ~Filtonary.inside);
            
            % Decomposition
            imMean = imrec ./ weights;
            imVar = im - imMean;
        end;
        
        function projW = projectionL1(W)
        % PROJECTION L1      - projection over L1 unit ball.
        %
        %   Usage:
        %       projW = Filtonary.projectionL1(W);
        %
        %   Arguments:
        %       W  (NxK): a matrix.
        %
        %   Return:
        %       projW   - projection of the column of W over L1 unit ball.

            % Dimensions
            [N,K] = size(W);
            L1 = 1;

            % Compute candidates threshold
            coefs = sort(abs(W), 1, 'descend');
            coefsSum = cumsum(coefs, 1);
            coefsCount = repmat((1:N)', [1, K]);
            candidates = (coefsSum - L1) ./ coefsCount;

            % Get thresholds
            L0 = sum(candidates < coefs, 1);
            candidates = [Inf(1,K); candidates];
            t = candidates(sub2ind(size(candidates), L0 + 1, 1:K));

            % Apply it
            t_mat = repmat(t, [N, 1]);
            projW = W - sign(W) .* t_mat;
            projW(abs(W) <= t_mat) = 0;

        end; 
        
    end;
    
    % Dependent properties
    methods
        function out = get.Nker(this)
            out = size(this.ker, 4);
        end;
        function out = get.Nc(this)
            out = size(this.ker, 3);
        end;
    end;
    
end
