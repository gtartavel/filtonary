function [y, r, Ic] = projL2w(x, w, type)
% PROJ L2 W     - projection on an ellipsoid.
%
%   Usage:
%       y = projL2(x);
%       y = projL2(x, w);
%       y = projL2(x, w, type);
%       [y, r, I] = projL2(x, w, type);
%
%   Arguments:
%       x           -- element to be projected
%       w = 1       -- weight of each dimension, or inverse of the radius
%       type = 1    -- project inside (1/true), on the border (0/false),
%                      or outside (-1) of the ellipsoid
%
%   Conditions:
%       all(w ~= 0)  -- please consider the subspace I = (w ~= 0) to ensure it
%
%   Return:
%       y = proj(x) = arg min ||y-x||^2
%             s.t.  ||w.*y||^2 <= 1 (type = 1)
%                   ||w.*y||^2 == 1 (type = 0)
%                   ||w.*y||^2 >= 1 (type = -1)
%
%   Non-unicity:
%       For type <= 0, the projection may be non-unique: it is a sphere.
%       In this case, the set of projection is
%           { y + r * u for u s.t. ||u(I)|| = rc and ||u(~I)|| = 0 }
%       r       -- radius of the sphere
%       I       -- support of the sphere

    % Mathematical justification for y = proj(x):
    %   * y is y(t) = x ./ (1 + t * w.^2) thanks to Lagrangian multipliers
    %       if all(x ~= 0) holds
    %   * dist(t) = sumL2(y(t)) is 1 on the ellipse border,
    %       by definition of the ellipse as the unit ball of weighted L2
    %   * dist decrease from dist(-1/w(k)^2) = Inf to dist(Inf) = 0
    %       where k = arg max(abs(w))
    %   * geometric consideration (involving symmetry) shows -1/w(k)^2 < t
    %       hence there is an unique t satisfying dist(t) == 1
    %   * dist_min(t) <= dist(t) <= dist_max(t) holds
    %       for dist_min(t) = ( w(k)*x(k) / (1 + t*w(k)^2) )^2
    %       and dist_max(t) = 1/t^2 * sumL2(x ./ w)
    %   * hence tmin <= t <= tmax wotj dist_min(tmin) = 1 = dist_max(tmax),
    %       so  tmin = abs(x(k) / w(k)) - 1 / w(k)^2
    %       and tmax = sqrt(sumL2(x ./ w))
    
    % Initialize
    y = x;
    r = 0;
    Ic = [];
    
    % Check arguments
    if (~exist('w', 'var'))
        w = 1;
    end;
    if (~exist('type', 'var'))
        type = 1;
    end;
    if (type <= 0 && nargout == 1) 
        warning('Projection may not be unique: you should handle the returned radius');
    end;

    % Compute the norm
    vec = @(v) v(:);
    sumL2 = @(v) sum(v(:).^2);
    d = sqrt(sumL2(w .* x));
    isEllipse = any(w(:) ~= w(1));
    
    % Compute the distance function
    if (isEllipse)
        [ww, wx] = deal(w.^2, w .* x);
        dist = @(t) sumL2(wx ./ (1 + t*ww)) - 1;
    end;
    
    % Project inside
    if (type >= 0 && d > 1)
        if (~isEllipse)
            y = x / d;
        else
            tmax = (1+1e-6) * sqrt(sumL2(x ./ w));
            t = fzero(dist, [0 tmax]);
            y = x ./ (1 + t * ww);
        end;
    end;
    
    % Project outside
    if (type <= 0 && d < 1)
        if (~isEllipse && d)
            y = x / d;
        elseif (~isEllipse)
            r = 1;
            Ic = true(size(x));
        else
            [w2max,k] = max(ww .* (x ~= 0));
            tmin = (1-1e-6) * abs(x(k) / w(k)) - 1 / ww(k);
            t = fzero(dist, [tmin 0]);
            y = x ./ (1 + t * ww);
            
            % If non-unique projections
            if (any(x(:) == 0))
                I = (wx ~= 0);
                wlist = unique(abs(w(ww > w2max)));
                w2ratio = bsxfun(@rdivide, vec(ww(I))', wlist(:).^2);
                r2circ = bsxfun(@minus, 1 ./ wlist(:).^2, ...
                    sum(bsxfun(@times, vec(x(I))'.^2, w2ratio ./ (1 - w2ratio).^2), 2));
                d2circ = bsxfun(@minus, 1 ./ wlist(:).^2, ...
                    sum(bsxfun(@times, vec(x(I))'.^2, w2ratio ./ (1 - w2ratio)), 2));
                [d2min,kcirc] = min(d2circ + (r2circ <= 0 | d2circ <= 0) * realmax);
                
                % Non-unique projection
                if (d2min < sumL2(x - y))
                    Ic = (abs(w) == wlist(kcirc));
                    y(~Ic) = x(~Ic) ./ (1 - ww(~Ic) / wlist(kcirc)^2);
                    r = sqrt(1 - sumL2(w .* y)) / wlist(kcirc);
                end;
            end;
        end;
    end;
    
    
end
