clear; clc;

% Parameters
tau = 12;       % size of the filters
N = 2;          % number of filters
lambda = 5;     % sparsity factor
niter = 100;    % number of iterations

% Load an image
imname = 'Fabric.0002.png';
im = double(imread(imname)) / 255;


%% Learn the dictionary

Fi = Filtonary(ones(tau, tau, size(im,3), N));  % initialize with ones
[F, W, rec] = Fi.learn(im, lambda, 2 * niter);  % learn the dictionary


%% Decompose the image

[W, rec] = F.decompose(im, lambda, niter);      % decomposition step
[W, opt] = F.optimize(im, W, niter);            % back-projection step


%% Display

% Original and reconstructed images
subplot(2, N+1, 1);   imshow(im);  title('Original');
subplot(2, N+1, N+2); imshow(opt); title('Reconstructed');

% Atoms and weights
sc = @(u) abs(u) / max(abs(u(:)));
for k = 1:N
    num = sprintf(' #%d', k);
    subplot(2, N+1, 0+k+1); imshow(sc(F.ker(:,:,:,k))); title(['Filtom' num]);
    subplot(2, N+1, N+k+2); imshow(sc(W(:,:,:,k)));     title(['Weights' num]);
end